console.log('Hello World');

let numA = parseInt(prompt('Provide a number'));
let numB = parseInt(prompt('Provide another number'));
let sum = numA + numB;
let difference = numA - numB;
let product = numA * numB;
let quotient = numA / numB;

if(numA + numB < 10){
	console.warn('The sum of the two numbers are: ' + sum);
}
else if(numA + numB >= 10 && numA + numB <=20){
	alert('The difference of the two numbers are: ' + difference);
}
else if(numA + numB >= 21 && numA + numB <=30){
	alert('The product of the two numbers are: ' + product);
}
else if(numA + numB >= 31){
	alert('The quotient of the two numbers are: ' + quotient);
}

let name = prompt('What is your name?');
let age = prompt('How old are you?');

let message1;

function messageAlert1(message1){
	if(name === '' || age === ''){
		return 'Are you a time traveler?';
	}
	else
		return 'Hello ' + name + '. Your age is ' + age;
}

message1 = messageAlert1(message1);
alert(message1)

let message2;

function isOfLegalAge(message2){
	if(age >= 18){
		return 'You are of legal age';
	}
	else
		return 'You are not allowed here';
}

message2 = isOfLegalAge(message2);
alert(message2);


switch(age){
	case '18':
		alert('You are now allowed to party');
		break;
	case '21':
		alert('You are now part of adult society');
		break;
	case '65':
		alert('We thank you for your contribution to society');
		break;
	default:
		alert('Are you sure you are not an alien?');
}

try{
	isOfLegalaAge(age);
} catch (error){
	console.warn(error.message);
} finally {
	isOfLegalAge(age);
}